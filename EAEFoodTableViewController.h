//
//  EAEFoodTableViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFoodTableViewController is used to show MyFoodstuff main food table
//  containing food names and kcal information.

#import <UIKit/UIKit.h>


@interface EAEFoodTableViewController : UITableViewController <UISearchResultsUpdating>

    // class to handle food table

@end


