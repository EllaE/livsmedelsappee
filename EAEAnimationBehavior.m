//
//  EAEAnimationBehavior.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEAnimationBehavior is used to in about view to allow consistent animating
//  with set of rules for gravity and collision behaviors.

#import "EAEAnimationBehavior.h"

@interface EAEAnimationBehavior()

@property (strong, nonatomic) UIGravityBehavior * theGravity;
@property (strong, nonatomic) UICollisionBehavior * theColision;

@end

@implementation EAEAnimationBehavior


/*
 Init creates object of EAEAnimationBehavior and adds to it child behaviors of
 gravity and collision that are even saved as the objects properties.
 */
-(instancetype)init{
    self = [super init];
    //add child behaviors
    [self addChildBehavior:self.theGravity];
    [self addChildBehavior:self.theColision];
    return self;

}

/*Returns theGravity property, creates one if property is null. */
-(UIGravityBehavior*)theGravity{
    if(! _theGravity){
        
        _theGravity = [[UIGravityBehavior alloc]init];
        _theGravity.magnitude = 0.9;
        
    }
    return _theGravity;
}

/*Returns theCollision property, creates one if property is null. */
-(UICollisionBehavior*)theColision{
    if(! _theColision){
        _theColision = [[UICollisionBehavior alloc]init];
        _theColision.translatesReferenceBoundsIntoBoundary = YES;               //boundary is superview!
    }
    return _theColision;
}

/*
 Method removeView adds given view item in argument from the EAEAnimationBehavior
 objects properties collision and gravity.
 */
-(void)addView: (UIView *) anyView{
    [self.theColision addItem:anyView];
    [self.theGravity addItem:anyView];
}

/*
 Method removeView removes given view item in argument from the EAEAnimationBehavior
 objects properties collision and gravity.
 */
-(void)removeView: (UIView *) anyView{
    [self.theColision removeItem:anyView];
    [self.theGravity removeItem:anyView];
}

/*
 Method removeView adds given item in argument from the EAEAnimationBehavior
 objects properties collision and gravity.
 */
-(void)addItem: (id <UIDynamicItem>) item{
    [self.theColision addItem:item];
    [self.theGravity addItem:item];
}

/*
 Method removeView removes given item in argument from the EAEAnimationBehavior
 objects properties collision and gravity.
 */
-(void)removeItem:(id <UIDynamicItem>) item{
    [self.theColision removeItem:item];
    [self.theGravity removeItem:item];
}


@end
