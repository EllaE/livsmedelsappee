//
//  EAEStartScreenViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-08.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEStartScreenViewController is class used for start screen of MyFoodstuff
//  app.

#import <UIKit/UIKit.h>

@interface EAEStartScreenViewController : UIViewController

@end
