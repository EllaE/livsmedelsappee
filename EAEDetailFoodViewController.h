//
//  EAEDetailFoodViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEDetailFoodViewController both faciliates showing of favorite food and of
//  food item from food table.

#import <UIKit/UIKit.h>
#import "EAEFood.h"

@interface EAEDetailFoodViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) EAEFood * foodItem;                               //EAEFood send with seque
@property ( nonatomic) BOOL isShowingFavoriteFood;

@end
