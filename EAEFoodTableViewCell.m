//
//  EAEFoodTableViewCell.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFoodTableViewCell is used for displaying main food table cell data.

#import "EAEFoodTableViewCell.h"

@implementation EAEFoodTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
