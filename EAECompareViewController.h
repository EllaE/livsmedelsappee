//
//  EAECompareViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAECompareViewController is used to allow user comparison of two EAEFood
//  item objects.

#import <UIKit/UIKit.h>



@interface EAECompareViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *graphFoodView;

@end
