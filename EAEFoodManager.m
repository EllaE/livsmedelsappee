//
//  EAEFoodManager.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEFoodManager class fetches food data from matapi, manages data flow for the
//  entire application and is made to be used as singleton.

#import "EAEFoodManager.h"




@interface EAEFoodManager()

@property (nonatomic,strong, readwrite) NSArray * allFoodList;
@property (nonatomic,strong, readwrite) NSArray * searchResultList;
@property (strong, nonatomic) NSMutableDictionary * foodPhotoDictionary;    //to save /load photo url data for key food items


@property (strong, nonatomic) NSMutableArray * fetchHelpingList;


-(void)fetchRequiredBaseData;
-(void)connectionForJSONNamedAndNumberDone;
-(void)fetchJSONShortNutriInfoForNumber:(NSNumber*)number;
-(NSArray*)sortFoodAlfabeticly:(NSArray*)listToSort;
-(void)fetchJSONNamesAndNumbers;
-(void)fetchJSONObjectFromNumber:(NSNumber*)number;

-(BOOL)checkIfFoodExists:(NSString*)foodName;

@end



@implementation EAEFoodManager

/*
 Use this public method to get instance of EAEFoodManager.
 */
+(id) sharedFoodManager{
    static EAEFoodManager * sharedFoodManager = nil;
    @synchronized(self) {
        if(sharedFoodManager ==nil){
            sharedFoodManager = [[self alloc]init];
        }
    }
    return sharedFoodManager;
}


/*
 As EAEFoodManager is singleton this method runs only once per application start
 and at its initation it runs method to fetch all food data from matapi.se
 */
-(instancetype)init{
    self = [super init];
    if(self){
        self.foodPhotoDictionary = [[NSMutableDictionary alloc]init];
        self.favoriteFoodListNumbers = [[NSMutableArray alloc]init];
        self.allFoodList = [[NSArray alloc]init];
        self.searchResultList = [[NSArray alloc]init];
        self.fetchHelpingList = [[NSMutableArray alloc]init];
        self.compareFood = [[NSMutableDictionary alloc]init];
        [self loadFavoriteFoodList];
        [self fetchRequiredBaseData];
    }
    return self;
}


//loads food number data from NSDefaults    // TODO - IMPLEMENT PICTURE URL LOAD (attach it when foodObj is made)
-(void) loadFavoriteFoodList{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    if([settings valueForKey:@"FavoriteFood"]){
        self.favoriteFoodListNumbers =[[settings valueForKey:@"FavoriteFood"] mutableCopy];
    }
    if([settings valueForKey:@"FoodPhotoURLDictionary"]){
        self.foodPhotoDictionary = [[settings valueForKey:@"FoodPhotoURLDictionary"]mutableCopy];
    }
    [settings synchronize];
}

//saves default!    TODO - IMPLEMENT PICTUREURL SAVING
-(void) saveFavoriteFoodList{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    [settings setObject:self.favoriteFoodListNumbers forKey:@"FavoriteFood"];
    [settings setObject:self.foodPhotoDictionary forKey:@"FoodPhotoURLDictionary"];
    [settings synchronize];
}

//this method starts chain of events based on notifications to get all base needed data for objects and create them
-(void)fetchRequiredBaseData{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(connectionForJSONNamedAndNumberDone)
                                                name:@"NSURLConnectionForNamesAndNumbersDidFinish"
                                              object:nil];
    [self fetchJSONNamesAndNumbers];
}

//when this event occured now foodList contains basic FoodItem objects with favorite, name and number info
-(void)connectionForJSONNamedAndNumberDone{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    self.allFoodList = [self sortFoodAlfabeticly:self.allFoodList];
}



/*
 Method loadShortNutriDataIfNecessaryFor food items number given in argument
 is used to dowbload kcal info for specific food item when displaying table view
 row with information about that food. This allows saving on internet transfers
 by only dowlnloading data that user is actually going to look at.
 */
-(void)loadShortNutriDataIfNecessaryFor:(NSNumber*)foodItemNr{  //kcal
    if([self searchObectWithIdNumber:foodItemNr]){              //if there is such item, download kcal data!
        [self fetchJSONShortNutriInfoForNumber:foodItemNr];
    }
}

/*
 Method loadShortNutriDataIfNecessaryFor food item given in argument
 is used to dowbload kcal info for specific food item when displaying table view
 row with information about that food. This allows saving on internet transfers
 by only dowlnloading data that user is actually going to look at.
 */
-(void)loadShortNutriDataIfNecessaryForFoodObj:(EAEFood*)foodObj{
    if([self.allFoodList containsObject:foodObj]){              // download kcal data!
        [self fetchJSONShortNutriInfoForNumber:foodObj.number];
    }
}


/*
 Method loadFoodDetailDataIfNecessaryFor food with number given in argument is
 used when a food item will be displayed with all its nutrition information and
 picture. This method completes EAEFood object that has number corresponding to
 foodItemNr by downloading detailed nutrition data and assigning picturePath info
 if there is any saved for that food item in the defaults.
 */
-(void)loadFoodDetailDataIfNecessaryFor:(NSNumber*)foodItemNr{                  //method unused, to be removed
    
    EAEFood * foodToComplete = [self searchObectWithIdNumber:foodItemNr];
    
    if(foodToComplete){                                                         //if there is such item, download kcal data!
        
        if(!foodToComplete.nutrientValues) {
            [self fetchJSONObjectFromNumber:foodItemNr];
        }
        
        if([self.favoriteFoodListNumbers containsObject:foodToComplete.number]){ //may be unnecessary! assigns it in food making
            foodToComplete.isFavorite = YES;
        }
        
        //here add URL check
        NSString * foodPictureUrl = [self.foodPhotoDictionary objectForKey:([foodItemNr stringValue])];
        if(foodPictureUrl){
            foodToComplete.picturePath = foodPictureUrl;
        }
    }
}

/*
 Method loadFoodDetailDataIfNecessaryForObj differs from loadFoodDetailDataIfNecessaryFor
 with only argument difference. This method takes an actual EAEFood object as
 argument.
 */
-(void)loadFoodDetailDataIfNecessaryForObj:(EAEFood*)foodItem{                  //get allNutrients, picturePath
    
    if([self.allFoodList containsObject:foodItem]){                             //if there is such item, download kcal data!
        NSString * index = [foodItem.number stringValue];
        NSString * picturePathData = [self.foodPhotoDictionary objectForKey:(index)];
        
        if(picturePathData){
            foodItem.picturePath = picturePathData;
        }
        
        if(!foodItem.nutrientValues) {
            
            [self fetchJSONObjectFromNumber:foodItem.number];
        } else {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"NSURLFoodDataFetchForNumberFinished" object:nil];
        }

    }
}

/*
 Method savePictureInformation sets picturePath property of givenEAEFood object.
 */
-(void)savePictureInformation:(NSString*)picturePath  forFood:(EAEFood*)foodItem{
    if(picturePath && foodItem){
        foodItem.picturePath = picturePath;
        NSString *indexKey = [foodItem.number stringValue];
        [self.foodPhotoDictionary setObject:picturePath forKey: indexKey];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PictureUpdated" object:nil];
        
    }
}


/*
 Method fetchJSONNamesAndNumbers fetches names and numbers of all items from matapi.se
 */
-(void)fetchJSONNamesAndNumbers{
    
    NSString * urlString = @"http://www.matapi.se/foodstuff";
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError * error){
                                                 
                                                 if(error){
                                                     NSLog(@"Error: %@", error);
                                                     return;
                                                 }
                                                 
                                                 NSError * parseError = nil;
                                                 id json = [NSJSONSerialization JSONObjectWithData:data
                                                                                           options:kNilOptions
                                                                                             error:&parseError];
                                                 
                                                 if(parseError){
                                                     NSLog(@"Error, fcouldn't retrieve food name and number %@", parseError);
                                                     self.allFoodList = nil;
                                                     return;
                                                 }
                                                 
                                                 NSMutableArray * foodObjects = [[NSMutableArray alloc]init];
                                                 NSDictionary * myJSON = json;
                                    
                                                 for(id n in myJSON){
                                                     EAEFood * foodItem;
                                                     if( n[@"number"] && n[@"name"] ){
                                                         NSString * name = [n objectForKey:@"name"];
                                                         NSNumber * number = [n objectForKey:@"number"];
                                                         if( name && number ){
                                                             
                                                             if([self.favoriteFoodListNumbers containsObject:number]){
                                                              
                                                                 foodItem = [[EAEFood alloc]initWithName:name
                                                                                        andWithNumber:number
                                                                                      andIfIsFavorite:YES];
                                                             } else{
                                                                 foodItem = [[EAEFood alloc]initWithName:name
                                                                                        andWithNumber:number
                                                                                      andIfIsFavorite:NO];
                                                             }
                                                              
                                                             [foodObjects addObject:foodItem];
                                                         }
                                                     }
                                                 }
                                                 
                                                 
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     
                                                     self.allFoodList = foodObjects;
                                                     
                                                     [[NSNotificationCenter defaultCenter]postNotificationName:@"NSURLConnectionForNamesAndNumbersDidFinish"
                                                                                                        object:nil];
                                                 });
                                                 
                                             }];
    [task resume];
    
}


/*
 Method fetchJSONObjectFromNumber fetches all food data for given food number.
 */
-(void)fetchJSONObjectFromNumber:(NSNumber*)number{
    
    NSString * urlString;
    urlString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", number];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError * error){
                                                 
                                                 if(error){
                                                     NSLog(@"Error: %@", error);
                                                     return;
                                                 }
                                                 
                                                 NSError * parseError = nil;
                                                 id json = [NSJSONSerialization JSONObjectWithData:data
                                                                                           options:kNilOptions
                                                                                             error:&parseError];
                                                 
                                                 if(parseError){
                                                     NSLog(@"Error, fcouldn't retrieve food name and number %@", parseError);
                                                     return;
                                                 }
                                                 
                                                 
                                                 NSDictionary * foodDictionary = json;
                                                 
                                                 NSDictionary * allNutritions = [foodDictionary objectForKey:@"nutrientValues"];
                                                 NSMutableDictionary * shortNutriInfo = [[NSMutableDictionary alloc]init];
                                                 [shortNutriInfo setObject:[allNutritions objectForKey:@"energyKcal"] forKey:@"energyKcal"];
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     //assign allnutritionstolist and shortvalue too if not present
                                                     EAEFood * foodToUpdate = [self searchObectWithIdNumber:number];
                                                     if(foodToUpdate){
                                                         foodToUpdate.nutrientValues = allNutritions;
                                                         if(!foodToUpdate.shortNutriInfo){
                                                             foodToUpdate.shortNutriInfo =[shortNutriInfo copy];
                                                         }
                                                     }
                                                     
                                                     [[NSNotificationCenter defaultCenter]postNotificationName:@"NSURLFoodDataFetchForNumberFinished" object:nil];
                                                     
                                                    // NSLog(@"Food data loaded for number: %d", foodNumber);
                                                     
                                                 });
                                                 
                                             }];
    [task resume];
}


/*
 Method fetchJSONShortNutriInfoForNumber fetches kcal infor for given food number
 to be used in main food table.
 */
-(void)fetchJSONShortNutriInfoForNumber:(NSNumber*)number{
    
    NSString * urlString;
    NSString * theNr = [number stringValue];
    urlString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@?nutrient=energyKcal", theNr ];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];

    NSURLSessionDataTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError * error){
                                                 
                                                 if(error){
                                                     NSLog(@"Error: %@", error);
                                                     return;
                                                 }
                                                 
                                                 NSError * parseError = nil;
                                                 id json = [NSJSONSerialization JSONObjectWithData:data
                                                                                           options:kNilOptions
                                                                                             error:&parseError];
                                                 
                                                 if(parseError){
                                                     NSLog(@"Error, fcouldn't retrieve food name and number %@", parseError);
                                                     return;
                                                 }
                                                 
                                                 
                                                 NSDictionary * foodDictionary = json;
                                                 NSString * value = [[foodDictionary objectForKey:@"nutrientValues"] objectForKey:@"energyKcal"];
                                                 NSMutableDictionary * shortNutriInfo = [[NSMutableDictionary alloc]init];
                                                 if(value){
                                                     [shortNutriInfo setObject:value forKey:@"energyKcal"];
                                                 }
                                                 
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     
                                                     EAEFood * foodToUpdate = [self searchObectWithIdNumber:number];
                                                     if(foodToUpdate){
                                                         if(!foodToUpdate.shortNutriInfo){
                                                             foodToUpdate.shortNutriInfo =[shortNutriInfo copy];
                                                         }
                                                     }
                                                      //NSLog(@"in the task main dispatch: %@ ", foodToUpdate.shortNutriInfo);
                                                     [[NSNotificationCenter defaultCenter]postNotificationName:@"NSURLShortNutriInfoDownloadFinished" object:nil];
                                                     
                                                     // NSLog(@"Food data loaded for number: %d", foodNumber);
                                                     
                                                 });
                                                 
                                             }];
    [task resume];
}




/*
 Method searchObjectWithIdNumber returns EAEFood object with property number
 matching given number in argument. If there is no match to that number then
 this method returns NULL.
 */
-(EAEFood*)searchObectWithIdNumber:(NSNumber*)foodItemNr{
    NSArray * filteredList;
    NSPredicate * predictatedFoodItem = [NSPredicate predicateWithFormat:@"number == %@",  foodItemNr];
    filteredList = [self.allFoodList filteredArrayUsingPredicate:predictatedFoodItem];
    if(filteredList[0]){
        return filteredList[0];
    } else {
        return NULL;
    }
}


/*
 Method addFoodTOFavoriteList adds given EAEFood objects number reference to 
 favorites list if its not present on that list.
 */

-(void) addFoodToFavoriteList:(EAEFood*)foodToAdd{
    if(![self.favoriteFoodListNumbers containsObject:foodToAdd.number]){
        [self.favoriteFoodListNumbers addObject:foodToAdd.number];
        foodToAdd.isFavorite = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"NSFavoriteItemUpdated" object:nil];
    }
}

/*
 Method removeFoodFromFavoriteList removes given EAEFood objects number reference
 from favorites list if its present on that list.
 */
-(void)  removeFoodFromFavoriteList:(EAEFood*)foodToRemove{
    if([self.favoriteFoodListNumbers containsObject:foodToRemove.number]){
        [self.favoriteFoodListNumbers removeObject:foodToRemove.number];
        foodToRemove.isFavorite = NO;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"NSFavoriteItemUpdated" object:nil];
    }
}



/*
 Method searchForFoodNamed takes input that is string and searches through all
 food list for any food with names containing that searched input. If it finds
 results then it assigns them to managers search result list.
 */
-(void)searchForFoodNamed:(NSString*)foodName{
    NSArray * filteredList;
    NSPredicate * predictatedFoodFind = [NSPredicate predicateWithFormat:@"name contains[c] %@",  foodName];
    filteredList = [self.allFoodList filteredArrayUsingPredicate:predictatedFoodFind];
    if(filteredList){
        self.searchResultList = [filteredList copy];
    }
}


/* 
 Method checkIfFoodExists returns true if given string name of food is to be found
 on food list for object of EAEFoodManager.
 */
-(BOOL)checkIfFoodExists:(NSString*)foodName{
    NSArray * filteredList;
    NSPredicate * predictatedFoodFind = [NSPredicate predicateWithFormat:@"name ==[c] %@",  foodName];
    filteredList = [self.allFoodList filteredArrayUsingPredicate:predictatedFoodFind];
    if(filteredList.count == 1){
        return YES;
    } else {
        return NO;
    }
}

/*
 This method takes an argument that is an array list of EAEFood items and then it
 sorts the list alfabeticly depending on the object name property.
 */
-(NSArray*)sortFoodAlfabeticly:(NSArray*)listToSort{
    
    NSArray * sortedList = [listToSort sortedArrayUsingComparator:^NSComparisonResult(id first, id second) {
        NSString *firstFood = [(EAEFood*)first name];
        NSString *secondFood = [(EAEFood*)second name];
        return [firstFood compare:secondFood];
    }];
    
    return sortedList;
}

/*
 Method compareTwoNSNUmber compares two NSNumber objects and returns the one being
 the one being bigger. UNUSED?
 */
-(NSNumber*)compareTwoNSNumber:(NSNumber*)firstNr andSecondNr:(NSNumber*)secondNr{
    long first = [firstNr longValue];
    long second = [secondNr longValue];
    if(first > second){
        return firstNr;
    } else {
        return secondNr;
    }
}

// FOOD COMPARE METHODS keys food1 and food2
/*
 Method addFoodToCompareNamed saves given food name in dictionary property compareFood
 depending if there are food to compare. FoodNr argument is to be either 1 or 2
 which allows adding compareFood value to right key - food1 or food2
 */
-(void)addFoodToCompareNamed:(NSString*)foodName atNumber:(int)foodNr{
    //do the add if possible (need to be empty etc)
    //inform
    if(foodNr == 1){
        NSString * food1Val = [self.compareFood valueForKey:@"food1"];
        if((food1Val && ![food1Val isEqualToString:foodName]) || !food1Val){
            [self.compareFood setValue:foodName forKey:@"food1"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"FoodComparePairUpdated" object:nil];
        }
    } else if(foodNr ==2){
        NSString * food2Val = [self.compareFood valueForKey:@"food2"];
        if((food2Val && ![food2Val isEqualToString:foodName]) || !food2Val){
            [self.compareFood setValue:foodName forKey:@"food2"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"FoodComparePairUpdated" object:nil];
        }
    }

}

/*
 CheckIfCompareEligible checks if dictionarty property named compareFood has two
 eligible string names for food comparison. Food is eligible for comparison if
 there are string values for keys food1, food2 and if names can be found on all
 food list.
 */
-(BOOL) checkIfCompareEligible{
    BOOL isComparable = NO;
    NSString * food1Name = [self.compareFood valueForKey:@"food1"];
    NSString * food2Name = [self.compareFood valueForKey:@"food2"];
    if(food1Name && food2Name){
        if([self checkIfFoodExists:food1Name] && [self checkIfFoodExists:food2Name]){
            isComparable = YES;
        }
    }
    return isComparable;
}


/*
 Method searchAndReturnFoodNamed returns EAEFood object with food name same as the
 one given in argument if such is present on all food list in food manager.
 */
-(EAEFood*)searchAndReturnFoodNamed:(NSString*)foodName{
    NSArray * filteredList;
    NSPredicate * predictatedFoodFind = [NSPredicate predicateWithFormat:@"name ==[c] %@",  foodName];
    filteredList = [self.allFoodList filteredArrayUsingPredicate:predictatedFoodFind];
    if(filteredList.count == 1){
        return filteredList[0];
    } else{
        return nil;
    }
}


@end

