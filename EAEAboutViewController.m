//
//  EAEAboutViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  This class handles about information for MyFoodstuff app.

#import "EAEAboutViewController.h"
#import "EAEFoodManager.h"
#import "EAEViewWithText.h"
#import "EAEAnimationBehavior.h"

@interface EAEAboutViewController ()

@property CGFloat sizeMultiplicator;
@property CGFloat circleSize1Multiplicator;
@property CGFloat circleSize2Multiplicator;
@property CGFloat circleSize3Multiplicator;
@property CGFloat rectangle1SizeMultiplicator;

@property (strong, nonatomic) EAEAnimationBehavior * animationBehavior;
@property (strong, nonatomic) UIDynamicAnimator * animator;

@property (weak, nonatomic) IBOutlet UIButton *playAnimationButton;


@end


@implementation EAEAboutViewController

-(UIDynamicAnimator*)animator{
    if(!_animator){
        _animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    }
    return _animator;
}

-(EAEAnimationBehavior*)animationBehavior{
    if(!_animationBehavior){
        _animationBehavior = [[EAEAnimationBehavior alloc]init];
        [self.animator addBehavior:_animationBehavior];
    }
    return _animationBehavior;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.circleSize1Multiplicator = 0.25;
    self.circleSize2Multiplicator = 0.15;
    self.circleSize3Multiplicator = 0.10;
    self.sizeMultiplicator = self.view.frame.size.width;
    
}

/*
 When playAnimationButtonClicked runs it starts method playAboutAnimation that
 launches animation showing info about MyFoodstuff app.
 */
- (IBAction)playAnimationButtonClicked:(UIButton *)sender {
    [self playAboutAnimation];
}

/* 
 Method playAboutAnimation runs PowerPoint alike presentation animation that drops
 in and slides in (first view is dropped with gravity and collision behavior but
 next one on top of it slides in) information about application.
 */
-(void)playAboutAnimation{
    
    self.playAnimationButton.hidden = YES;
    
    CGRect rectangleF = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, (self.view.frame.size.height /1.15));
    __block CGRect rectangleF2;
    
    __block UIImageView * kvadrat1 = [[UIImageView alloc]initWithFrame:rectangleF];
    [kvadrat1 setImage:([UIImage imageNamed:@"smallB1"])];
    
    
    __block UITextView * author;
    NSMutableString * text = [@"Author: Ella Einarsen\n\nApp made with help of matapi.se\n\nImage used as background in about comes from: http://freedesignfile.com/ and image used as start screen comes from freepik.com" mutableCopy];
    
    
        [UIImageView animateWithDuration:2
                               delay:0
                             options:(UIViewAnimationOptionBeginFromCurrentState)
                          animations:^{
                              [self.view addSubview:kvadrat1];
                              [self.animationBehavior addItem:kvadrat1];
                              
                          } completion:^(BOOL finished) {
                             rectangleF2 = CGRectMake(kvadrat1.frame.origin.x, kvadrat1.frame.origin.y, kvadrat1.frame.size.width -30, (kvadrat1.frame.size.height -30));
                              author = [[UITextView alloc]initWithFrame:rectangleF2];
                              author.center = kvadrat1.center;
                              author.text = text;
                              [author setEditable:NO];
                              [author setTextColor:[UIColor whiteColor]];
                              [author setAlpha:1.0f];
                              
                          }];
    
    [UIImageView animateWithDuration:3
                          delay:0
                        options:(UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         [kvadrat1 setAlpha:0.6f];
    } completion:^(BOOL finished) {
        
        [UIImageView animateWithDuration:5
                                   delay:0
                                 options:(UIViewAnimationOptionBeginFromCurrentState)
                              animations:^{
                                  [self.view addSubview:author];
                                  author.center = kvadrat1.center;
                                  
                              } completion:^(BOOL finished) {
                                  [author setTextColor:[UIColor blackColor]];
                                  
                              }];
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end



