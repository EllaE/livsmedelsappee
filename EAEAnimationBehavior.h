//
//  EAEAnimationBehavior.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEAnimationBehavior is used to in about view to allow consistent animating
//  with set of rules for gravity and collision behaviors.

#import <UIKit/UIKit.h>

@interface EAEAnimationBehavior : UIDynamicBehavior


-(void)addView: (UIView *) anyView;
-(void)removeView: (UIView *) anyView;

-(void)addItem: (id <UIDynamicItem>) item;
-(void)removeItem:(id <UIDynamicItem>) item;

@end
