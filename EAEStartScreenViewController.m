//
//  EAEStartScreenViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-08.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEStartScreenViewController is class used for start screen of MyFoodstuff
//  app.

#import "EAEStartScreenViewController.h"

@interface EAEStartScreenViewController ()

@end

@implementation EAEStartScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
