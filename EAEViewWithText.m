//
//  EAEViewWithText.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//

#import "EAEViewWithText.h"

@interface EAEViewWithText()

@property (strong,nonatomic)UITextView * textView;

-(void)createAndAddTextView;

@end

@implementation EAEViewWithText

/* 
 Method modifyViewWithText takes string and color as arguments and modifies the
 object of EAEViewWithText to show that text and to have given background color.
 */
-(void)modifyViewWithText:(NSString*)text  andColor:(UIColor*)color{
    self.backgroundColor = color;
    self.layer.cornerRadius = 10.0f;
    self.textForView = text;
    [self createAndAddTextView]; //creates and adds ram depending on frame
}


/*
 Method createAndAddTextView creates text view with text from property textforView
 and adds that view in the middle of object of EAEViewWithText.
 */
-(void)createAndAddTextView{
    UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(20, 20, self.bounds.size.width -(10), self.bounds.size.height-(10))];
    tv.text = self.textForView;
    tv.layer.cornerRadius = 10.0f;
    tv.backgroundColor = [([UIColor whiteColor]) colorWithAlphaComponent:0.9f];
    tv.center = self.center;
    tv.editable = NO;
    
    [self addSubview:tv];

}

@end
