//
//  EAEFood.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFood holds chosen food information received from matapi.se. All
//  values are given per 100g of that specific food.

#import <Foundation/Foundation.h>

@interface EAEFood : NSObject 

@property (strong, nonatomic, readonly) NSString * name;
@property (strong, nonatomic, readonly) NSNumber * number;
@property (nonatomic) BOOL isFavorite;
@property (strong, nonatomic) NSDictionary* shortNutriInfo; //for main table view
@property (strong, nonatomic) NSDictionary* nutrientValues; //for detail view
@property (nonatomic, strong) NSString * picturePath;

-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId;

-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId
            andIfIsFavorite:(BOOL)favorite;

-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId
            andIfIsFavorite:(BOOL)favorite
      andWithShortValueInfo:(NSDictionary*)shortInfo
          andNutrientValues:(NSDictionary*)valuePairs
            andWithImageUrl:(NSString*)picturePath;

-(NSString*)nutritionDataToPrint;
-(NSString*)getFoodHealthValue;


@end

