//
//  EAECompareResultViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-04-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//

#import "EAECompareResultViewController.h"
#import "EAEViewWithText.h"

@interface EAECompareResultViewController ()

@property (strong, nonatomic) EAEFoodManager * foodManager;
@property (strong, nonatomic) EAEFood * food1;
@property (strong, nonatomic) EAEFood * food2;
@property (nonatomic) BOOL graphActive;

-(void)updateAfterNotification;

@end

@implementation EAECompareResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSString * food1Name = [self.foodManager.compareFood valueForKey:@"food1"];
    NSString * food2Name = [self.foodManager.compareFood valueForKey:@"food2"];
    self.title = self.title = @"Food Compare";
    self.food1 = [self.foodManager searchAndReturnFoodNamed:food1Name];
    self.food2 = [self.foodManager searchAndReturnFoodNamed:food2Name];
    [self.foodManager loadFoodDetailDataIfNecessaryFor:self.food1.number];
    [self.foodManager loadFoodDetailDataIfNecessaryFor:self.food2.number];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAfterNotification) name:@"NSURLFoodDataFetchForNumberFinished" object:nil];
    if([self.food1.nutrientValues valueForKey:@"fat"] && [self.food2.nutrientValues valueForKey:@"fat"]){
        [self showNewGraph];
        self.graphActive = YES;
    }

}

/* 
 Method updateAfterNotification runs when food nutrition data have been downloaded
 if it was necessary to download it.
 */
-(void)updateAfterNotification{
    if(!self.graphActive){
        [self showNewGraph];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(EAEFoodManager*)foodManager{
    _foodManager = [EAEFoodManager sharedFoodManager];
    return _foodManager;
}

/*
 Method showNewGraph shows graph wihich visualises compare food items procent
 amount of carbohydrates, fat and protein per 100g of food. Food is organised in
 two colors, blue and yellow to visualise first and second food item.
 */
-(void)showNewGraph{
    
    self.graphV = [[GKBarGraph alloc]initWithFrame:self.view.frame];
    
    self.graphV.backgroundColor = [UIColor whiteColor];
    self.graphV.dataSource = self;
    [self.graphV draw];
    [self.view addSubview:self.graphV];
    
    CGRect labelFrame = self.view.frame;
    labelFrame.size.height = labelFrame.size.height / 8;
    labelFrame.origin.y = labelFrame.origin.y +50;
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    NSString * textTitle = [NSString stringWithFormat:@"Blue: %@, Yellow: %@", self.food1.name, self.food2.name];
    [titleLabel setText:textTitle];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.graphV addSubview:titleLabel];
    
    CGRect infoFrame = self.view.frame;
    infoFrame.size.height = infoFrame.size.height / 8;
    infoFrame.origin.y = infoFrame.origin.y + titleLabel.frame.size.height + 15;
    UILabel * diagramInfo = [[UILabel alloc] initWithFrame:infoFrame];
    [diagramInfo setBackgroundColor:[UIColor clearColor]];
    NSString * diagramInfoText = @"1: fat 2: protein 3: carbs";
    [diagramInfo setText:diagramInfoText];
    [diagramInfo setAdjustsFontSizeToFitWidth:YES];
    [self.graphV addSubview:diagramInfo];
}

-(NSInteger)numberOfBars{
    return 6;
}

-(NSNumber*)valueForBarAtIndex:(NSInteger)index{
    NSNumber * compareValue = @0;
    switch (index){
        case 0:
            compareValue = @([[self.food1.nutrientValues valueForKey:@"fat"] intValue]);
            break;
        case 1:
            compareValue = @([[self.food2.nutrientValues valueForKey:@"fat"] intValue]);
            break;
        case 2:
            compareValue = @([[self.food1.nutrientValues valueForKey:@"protein"] intValue]);
            break;
        case 3:
            compareValue = @([[self.food2.nutrientValues valueForKey:@"protein"] intValue]);
            break;
        case 4:
            compareValue = @( [[self.food1.nutrientValues valueForKey:@"carbohydrates"] intValue]);
            break;
        case 5:
            compareValue = @([[self.food2.nutrientValues valueForKey:@"carbohydrates"] intValue]);
            break;
        default: break;
    }
    return compareValue;
}

-(UIColor*) colorForBarAtIndex:(NSInteger)index{
    UIColor * f1Color = [UIColor blueColor];
    UIColor * f2Color = [UIColor yellowColor];
    UIColor * barColor = f1Color;
    if(index == 1 || index == 3 || index == 5 ){
        barColor = f2Color;
    }
    return barColor;
}

-(NSString*)titleForBarAtIndex:(NSInteger)index{
    NSString * compareName = @"";
    switch (index){
        case 0:
            compareName = @"1";
            break;
        case 1:
             compareName = @"1";
            break;
        case 2:
             compareName = @"2";
            break;
        case 3:
             compareName = @"2";
            break;
        case 4:
             compareName = @"3";
            break;
        case 5:
             compareName = @"3";
            break;
        default:
            break;
    }
    return compareName;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
