//
//  EAEFoodTableViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFoodTableViewController is used to show MyFoodstuff main food table
//  containing food names and kcal information.

#import "EAEFoodTableViewController.h"
#import "EAEFoodTableViewCell.h"
#import "EAEFoodManager.h"
#import "EAEFood.h"
#import "EAEDetailFoodViewController.h"

@interface EAEFoodTableViewController ()


@property (strong, nonatomic) UISearchController * searchController;
@property (strong, nonatomic) EAEFoodManager * foodManager;

-(void)updateAfterNotification:(NSNotification*)notif;
-(void)updateCellKcalWhenReady;

@end

@implementation EAEFoodTableViewController

//using singleton
-(EAEFoodManager*)foodManager{
    _foodManager = [EAEFoodManager sharedFoodManager];
    return _foodManager;
}

-(UISearchController*)searchController{
    if(!(_searchController)){
        self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.searchController.searchResultsUpdater = self;
        self.definesPresentationContext =YES;
        self.searchController.dimsBackgroundDuringPresentation = NO;
    }
    return _searchController;
}



- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAfterNotification:) name:@"NSURLConnectionForNamesAndNumbersDidFinish" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateCellKcalWhenReady) name:@"NSURLShortNutriInfoDownloadFinished" object:nil];
    
}

/*
 Method updateAfterNotification is called once when all food names with their
 respective numbers have been downloaded and sorted alfabeticly. It updates table
 by reloading data.
 */
-(void)updateAfterNotification:(NSNotification*)notif{
    [self.tableView reloadData];
    
}

/*
 Method updateCellKcalWhenReady is called every time when kcal data for food item
 is downloaded. This happens when user scrolls through application as short kcal
 information is downloaded only then to spare data connection. It reloads view data
 to show kcal data.
 */
-(void)updateCellKcalWhenReady{
    [self.tableView reloadData];
}

/*
 Method updateSearchResultsForSearchController is used to show search view of table
 when user is using searchfield to look after specific food.
 */
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString * searchFood = self.searchController.searchBar.text;
    //NSLog(@"Search text = %@",searchFood);
    if(searchFood.length >0){
        [self.foodManager searchForFoodNamed:searchFood];
    }
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   if(self.searchController.isActive && self.searchController.searchBar.text.length >0){
       return self.foodManager.searchResultList.count;
   } else {
       return self.foodManager.allFoodList.count;
   }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EAEFoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"singleCell" forIndexPath:indexPath];
    
    EAEFood * food;
    if(self.searchController.isActive && self.searchController.searchBar.text.length >0){
        food = self.foodManager.searchResultList[indexPath.row];
    } else {
        food = self.foodManager.allFoodList[indexPath.row];

    }
    
    cell.foodNameLabel.text = food.name;
    cell.kcalInfoLabel.text= @"kcal";
    if(!food.shortNutriInfo){
        [self.foodManager loadShortNutriDataIfNecessaryForFoodObj:food];
        cell.extraDataLabel.text = @"-";

    } else {
        cell.extraDataLabel.text = [NSString stringWithFormat:@"%@", food.shortNutriInfo[@"energyKcal"]];
    }
    
    cell.viewedFood = food;
    return cell;
}

 
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"foodListSegue"]){
        EAEFoodTableViewCell * actualCell = sender;
        EAEDetailFoodViewController * detailViewToStart = [segue destinationViewController];
        detailViewToStart.foodItem = actualCell.viewedFood;
        detailViewToStart.isShowingFavoriteFood = NO;   //indicates if detail view will have some extras that only shows for favs

    }
    
}


@end
