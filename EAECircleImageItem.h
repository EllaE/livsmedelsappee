//
//  UICircleImageItem.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
// Class EAECircleImageItem is used to create round image view.

#import <UIKit/UIKit.h>

@interface EAECircleImageItem : UIImageView 



-(void)modifyToBeCircleWithDiameter:(int)diameter andWithImage:(UIImage*)img;



@end
