//
//  EAEFood.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFood holds chosen food information received from matapi.se. All
//  values are given per 100g of that specific food.

#import "EAEFood.h"

@interface EAEFood()

@property (strong, nonatomic, readwrite) NSString * name;
@property (strong, nonatomic, readwrite) NSNumber * number;

@end

@implementation EAEFood

/*
 This init requires all food properties to be given in arguments to create a Food
 item. 
 */
-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId
            andIfIsFavorite:(BOOL)favorite
      andWithShortValueInfo:(NSDictionary*)shortInfo
          andNutrientValues:(NSDictionary*)valuePairs
            andWithImageUrl:(NSString*)picturePath;{
    
    self = [super init];
    if(self){
        self.name = name;
        self.number = itemId;
        self.nutrientValues = valuePairs;
        self.isFavorite = favorite;
        self.shortNutriInfo = shortInfo;
        self.picturePath = picturePath;
    }
    return self;

}

/*
 This init requires name, number and favorite arguments information to create 
 food item, it sets any other values as default.
 */
-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId
            andIfIsFavorite:(BOOL)favorite{
    
    return [self initWithName:name
                andWithNumber:itemId
              andIfIsFavorite:favorite
        andWithShortValueInfo:NULL
             andNutrientValues:NULL
              andWithImageUrl:NULL];
}

/*
 This init requires food item name and id number to create Food item, all other 
 values are default. To be used when food list is first downloaded after program
 launches and downloads food data from matapi.se
 */
-(instancetype)initWithName:(NSString*)name
              andWithNumber:(NSNumber*)itemId
            {
    
    return [self initWithName:name
                andWithNumber:itemId
              andIfIsFavorite:NO];
}

/*
 Method description returns objects name and number.
 */
-(NSString*)description{
    return [NSString stringWithFormat:@"name: %@, number: %@", self.name, self.number];
}

/*
 Method nutritionDataToPrint returns NSString with information about all the
 nutritions and values that are contained in dictionary nutrientValues for the
 food object.
 */
-(NSString*)nutritionDataToPrint{
    NSMutableString * nutrientInfo = [@"Nutrient values:\n" mutableCopy];
    for (id  key in self.nutrientValues){
        [nutrientInfo appendString:[NSString stringWithFormat:@"%@: %@\n", key,
                                    [self.nutrientValues objectForKey:key]]];
    }
    return [nutrientInfo copy];
}

/*
 Method getFoodHealthValue returns health information of specific Food item which 
 is decided upon amount of carbohydrates per 100g of that food. Returns health
 value as string.
 */
-(NSString*)getFoodHealthValue{
    NSString * healthValue;
    if(self.nutrientValues){
        double carbs = [[self.nutrientValues objectForKey:@"carbohydrates"] doubleValue];
        
        if(carbs < 10){
            healthValue = @"Very healthy";
        } else if (carbs < 20){
            healthValue = @"Healthy";
        } else if (carbs < 30){
            healthValue = @"Quite healthy";
        } else {
            healthValue = @"Avoid";
        }
    } else {
        healthValue = @"Not available";
    }
    return healthValue;
}



@end
