//
//  EAEViewWithText.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
// Class EAEViewWithText is used to create a rectangle view with smaller text view
// inside of it.

#import <UIKit/UIKit.h>

@interface EAEViewWithText : UIView <UIDynamicItem>


@property (strong, nonatomic)NSString * textForView;

-(void)modifyViewWithText:(NSString*)text  andColor:(UIColor*)color;


@end
