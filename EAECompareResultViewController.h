//
//  EAECompareResultViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-04-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAECompareResultViewController shows comparison diagram for two food items.

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>
#import "EAEFoodManager.h"
#import "EAEFood.h"

@interface EAECompareResultViewController : UIViewController <GKBarGraphDataSource>
@property(nonatomic) GKBarGraph * graphV;

@end
