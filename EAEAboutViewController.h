//
//  EAEAboutViewController.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  This class handles about information for MyFoodstuff app.

#import <UIKit/UIKit.h>

@interface EAEAboutViewController : UIViewController


@end
