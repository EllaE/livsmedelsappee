//
//  EAECompareViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAECompareViewController is used to allow user comparison of two EAEFood
//  item objects.

#import "EAEFoodManager.h"
#import "EAECompareViewController.h"
#import "EAECompareResultViewController.h"

@interface EAECompareViewController ()

@property (strong, nonatomic) EAEFoodManager * foodManager;
@property (weak, nonatomic) IBOutlet UITextField *food1TextField;
@property (weak, nonatomic) IBOutlet UITextField *food2TextField;

@property(nonatomic) GKBarGraph * graphV;

-(void)whenCompareNameDataUpdate;
-(void)displayMessage:(NSString*)messageText withTitle:(NSString*)title;

@end

@implementation EAECompareViewController

-(EAEFoodManager*)foodManager{
    _foodManager = [EAEFoodManager sharedFoodManager];
    return _foodManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     self.food1TextField.delegate = self;
     self.food2TextField.delegate = self;
    //need to add updater on EAEFoodManager every time these pair is set
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(whenCompareNameDataUpdate)
                                                name:@"FoodComparePairUpdated" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    if(self.foodManager.compareFood){
        NSLog(@"Show info %@", [self.foodManager.compareFood valueForKey:@"food1"]);
    }
}


/* 
 Method compareClicked run when user clicks compare button. It checks if comparison
 is eligible and if that is the case then it starts new activity showing diagram.
 */
- (IBAction)compareClicked:(UIButton *)sender {
    NSLog(@"Compare: %@ field1, %@ field2", self.food1TextField.text, self.food2TextField.text);
    NSString * f1 = self.food1TextField.text;
    NSString * f2 = self.food2TextField.text;
    [self.foodManager addFoodToCompareNamed:f1 atNumber:1];
    [self.foodManager addFoodToCompareNamed:f2 atNumber:2];
    
    if(self.foodManager.compareFood && [self.foodManager.compareFood valueForKey:@"food1"] && [self.foodManager.compareFood valueForKey:@"food2"]){
        if([self.foodManager checkIfCompareEligible]){
            NSLog(@"Compare eligible");

            EAECompareResultViewController *viewController = [[EAECompareResultViewController alloc] init];
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
            
            
        } else {
            NSLog(@"Compare NOT eligible");
            [self displayMessage:@"Comparison food names not eligible. Food names correctly spelled must be present." withTitle:@"Message"];
            NSLog(@"%@ f1, %@ f2", [self.foodManager.compareFood valueForKey:@"food1"],[self.foodManager.compareFood valueForKey:@"food2"]);
        }
        
    } else {
        [self displayMessage:@"Must be two food names to compare." withTitle:@"Message"];

    }
}

/* 
 Method whenCompareNameDataUpdate checks if values of compareFood in EAEFoodManager 
 object are same as the ones displayed in text fields corresponding to value input.
 If they arent then text fields are updated.
 */
-(void)whenCompareNameDataUpdate{
    if(self.foodManager.compareFood){
        // reset view with comparison to empty, update textFields if they arent
        // same as foodmanagers

        if([self.foodManager.compareFood valueForKey:@"food1"] && (![[self.foodManager.compareFood valueForKey:@"food1"] isEqualToString:self.food1TextField.text])){
            self.food1TextField.text = [self.foodManager.compareFood valueForKey:@"food1"];

        }
        
        if([self.foodManager.compareFood valueForKey:@"food2"] && (![[self.foodManager.compareFood valueForKey:@"food2"] isEqualToString:self.food2TextField.text])){
            self.food2TextField.text = [self.foodManager.compareFood valueForKey:@"food2"];
        }
        
    }
}



/*
 Method displayMessage displays message with text and title given by methods 
 arguments.
 */
-(void)displayMessage:(NSString*)messageText withTitle:(NSString*)title{
    UIAlertController * message =   [UIAlertController
                                          alertControllerWithTitle:title
                                          message:messageText
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [message dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [message addAction:ok];
    [self presentViewController:message animated:YES completion:nil];
}


/*
 Method textFieldShouldReturn allows hiding keyboard when editing of text field is
 finished and user clicked on return button.
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
