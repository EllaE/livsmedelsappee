//
//  EAEFoodTableViewCell.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  Class EAEFoodTableViewCell is used for displaying main food table cell data.

#import <UIKit/UIKit.h>
#import "EAEFood.h"

@interface EAEFoodTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *kcalInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *foodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *extraDataLabel;

@property (strong, nonatomic) EAEFood * viewedFood;

@end
