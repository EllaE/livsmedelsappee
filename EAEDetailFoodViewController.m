//
//  EAEDetailFoodViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-27.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEDetailFoodViewController both faciliates showing of favorite food and of
//  food item from food table.

#import "EAEDetailFoodViewController.h"
#import "EAEFoodManager.h"

@interface EAEDetailFoodViewController ()

@property (weak, nonatomic) IBOutlet UILabel *foodNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *foodInfoTextView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIImageView *foodImageView;

//health value
@property (weak, nonatomic) IBOutlet UILabel *healthValueLabel;
@property (weak, nonatomic) IBOutlet UIStackView *healthValueStackView;


@property (strong, nonatomic) EAEFoodManager * foodManager;

//@property (strong, nonatomic) UIImage * foodImage;

-(void)displayFoodData;
-(void)updateFavoriteButtonImage;
-(void)updateAfterNotification;
-(void)pictureUpdatedNotification;

-(void)loadExistingFoodImage;
-(NSString*)imagePathForFileNamed:(NSString*)fileName;

-(void)displayHealthValueMessage;


@end


@implementation EAEDetailFoodViewController

//using singleton
-(EAEFoodManager*)foodManager{
    _foodManager = [EAEFoodManager sharedFoodManager];
    return _foodManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //possible differences between normal detail view and fav view..
    if(self.isShowingFavoriteFood){
        //[self.favoriteButton setHidden:YES];
    } else {
        //[self.foodImageView setHidden:YES];
        
    }
    
   self.foodInfoTextView.layer.borderWidth = 2.0f;
    
    self.foodInfoTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    CALayer *upperBorder2 = [CALayer layer];
    upperBorder2.backgroundColor = [[UIColor grayColor] CGColor];
    upperBorder2.frame = CGRectMake(0, 0, CGRectGetWidth(self.foodImageView.frame), 1.0f);
    [self.foodImageView.layer addSublayer:upperBorder2];
    
    
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor grayColor] CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(self.healthValueStackView.frame), 1.0f);
    [self.healthValueStackView.layer addSublayer:upperBorder];
    
    
    
    if(self.foodItem){
        [self displayFoodData];
        [self.foodManager loadFoodDetailDataIfNecessaryForObj:self.foodItem];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAfterNotification) name:@"NSURLFoodDataFetchForNumberFinished" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pictureUpdatedNotification) name:@"PictureUpdated" object:nil];
        
    } else {                                                                    //no data exists
        self.foodNameLabel.text = @"No food data found :(";
        [self.favoriteButton setHidden:YES];
        [self.cameraButton setHidden:YES];
    }
    
}

- (IBAction)showHealthInfoClicked:(UIButton *)sender {
    [self displayHealthValueMessage];
}

-(void)displayHealthValueMessage{
    UIAlertController * healthMessage=   [UIAlertController
                                  alertControllerWithTitle:@"Health Value Information"
                                  message:@"Health value is assigned depending on amount of carbohydrates per 100g in nutrition. \n\n < 10g - Very healthy \n < 20g - Healthy \n < 30g - Quite Healthy \n > 30g - Avoid.\n"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [healthMessage dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [healthMessage addAction:ok];
    [self presentViewController:healthMessage animated:YES completion:nil];
}



-(void)updateAfterNotification{
    NSDictionary * foodDetailNutritions = self.foodItem.nutrientValues;
    if(foodDetailNutritions){
        self.foodInfoTextView.text = [self.foodItem nutritionDataToPrint];
        self.healthValueLabel.text = [self.foodItem getFoodHealthValue];
    } else {
        self.foodInfoTextView.text = @"No data found:(";
    }
    if(self.foodItem.picturePath){
        [self loadExistingFoodImage];
    }
    [self.view reloadInputViews];
}

//called after a picture was updated at run time, person took new one
-(void)pictureUpdatedNotification{
    if(self.foodItem.picturePath){
         [self loadExistingFoodImage];
    }
}

/*
 Displays food item title, nutrition values and picture (TODO) if fooditem object
 is not null. Otherwise displays error message on the page.
 */
-(void)displayFoodData{
    
    self.foodNameLabel.text = self.foodItem.name;
    if(!self.foodItem.nutrientValues){
        self.foodInfoTextView.text = @"Loading...";
    } else {
         self.foodInfoTextView.text = [self.foodItem nutritionDataToPrint];
        self.healthValueLabel.text = [self.foodItem getFoodHealthValue];
    }
    if(self.foodItem.picturePath){
         [self loadExistingFoodImage];
    }
    [self updateFavoriteButtonImage];

    
}

-(void)updateFavoriteButtonImage{
    if(self.foodItem.isFavorite){
        [self.favoriteButton setImage:[UIImage imageNamed:@"star-b"] forState:UIControlStateNormal];
    } else {
        [self.favoriteButton setImage:[UIImage imageNamed:@"star-a"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)changeFavoriteStatus:(UIButton *)sender {
    if(self.foodItem.isFavorite){
        [self.foodManager removeFoodFromFavoriteList:self.foodItem];
    } else {
        [self.foodManager addFoodToFavoriteList:self.foodItem];
    }

    [self updateFavoriteButtonImage];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"NSFavoriteItemUpdated" object:nil];
}


- (IBAction)takePhoto:(UIButton *)sender {
    [self prepareForPicture];
}

-(void)viewWillAppear:(BOOL)animated{
    [self updateFavoriteButtonImage];
    //self updatePicture
}




/*Camera methods */

-(void)prepareForPicture{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    
    [self presentViewController:imagePicker animated:YES completion:nil];   //can add block on complete
    
    //NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    } else if ([UIImagePickerController
                isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
    
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}



-(void)imagePickerController:(UIImagePickerController *)picker
                didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * image = info[UIImagePickerControllerEditedImage];
    self.foodImageView.image = image;
    
    //save picture locally in special folder
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSDateFormatter *niceDateFormat=[[NSDateFormatter alloc] init];
    [niceDateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString * actualDate = [niceDateFormat stringFromDate:[NSDate date]];
    NSString * pictureName = [NSString stringWithFormat:@"%@%@.png", self.foodItem.name, actualDate];
    NSString *wholePath = [self imagePathForFileNamed:pictureName];
    BOOL isSaved = [imageData writeToFile:wholePath atomically:YES];
    if(isSaved){
        NSLog(@"File saved ,%@", wholePath);
        
        [self.foodManager savePictureInformation:pictureName forFood:self.foodItem];
    } else {
        NSLog(@"Failed to save image:(((");
    }
    
}

-(NSString*)imagePathForFileNamed:(NSString*)fileName{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = paths[0];
    return [path stringByAppendingPathComponent:fileName];
}



//to be used when loads existing pic from url at food object
-(void)loadExistingFoodImage{
    NSString * wholeImagePath = [self imagePathForFileNamed: self.foodItem.picturePath];
    NSLog(@"Whole path: %@", wholeImagePath);
    UIImage * foodImage = [UIImage imageWithContentsOfFile:wholeImagePath];
   
    if(foodImage){
         self.foodImageView.image = foodImage;
    } else {
        NSLog(@"No success");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
