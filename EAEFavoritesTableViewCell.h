//
//  EAEFavoritesTableViewCell.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-28.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEFavoritesTableViewCell is used to display cell in table with more detailed
//  information which is name, image, kcal, carbs and fat levels.

#import <UIKit/UIKit.h>
#import "EAEFood.h"

@interface EAEFavoritesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (weak, nonatomic) IBOutlet UILabel *favoriteFoodLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoriteKcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoriteCarbsLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoriteFatLabel;

@property (strong, nonatomic) EAEFood * viewedFood;
@end
