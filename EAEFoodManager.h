//
//  EAEFoodManager.h
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-03.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEFoodManager class fetches food data from matapi, manages data flow for the
//  entire application and is made to be used as singleton.

#import <Foundation/Foundation.h>
#import "EAEFood.h"

@interface EAEFoodManager : NSObject

@property (nonatomic,strong, readonly) NSArray * allFoodList;
@property (nonatomic,strong, readonly) NSArray * searchResultList;
@property (strong, nonatomic) NSMutableArray * favoriteFoodListNumbers;     //to save/load

@property (strong, nonatomic) NSMutableDictionary * compareFood;    // food1, food2

-(BOOL) checkIfCompareEligible;
-(NSArray*) findComparedFood;

-(void)addFoodToCompareNamed:(NSString*)foodName atNumber:(int)foodNr;

- (instancetype)init NS_UNAVAILABLE; //singleton
+(id) sharedFoodManager;


-(void) saveFavoriteFoodList;
-(void) loadFavoriteFoodList;

-(void) addFoodToFavoriteList:(EAEFood*)foodToAdd;
-(void) removeFoodFromFavoriteList:(EAEFood*)foodToRemove;
-(void)savePictureInformation:(NSString*)picturePath  forFood:(EAEFood*)foodItem;

-(void)loadFoodDetailDataIfNecessaryFor:(NSNumber*)foodItemNr;
-(void)loadShortNutriDataIfNecessaryFor:(NSNumber*)foodItemNr;

-(void)loadShortNutriDataIfNecessaryForFoodObj:(EAEFood*)foodObj;
-(void)loadFoodDetailDataIfNecessaryForObj:(EAEFood*)foodItem;

-(void)searchForFoodNamed:(NSString*)foodName;
-(EAEFood*)searchAndReturnFoodNamed:(NSString*)foodName;
-(EAEFood*)searchObectWithIdNumber:(NSNumber*)foodItemNr;

@end
