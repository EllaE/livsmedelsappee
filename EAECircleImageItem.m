//
//  UICircleImageItem.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-03-12.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//

#import "EAECircleImageItem.h"

@interface EAECircleImageItem()

@property (nonatomic) int circleDiameter;

@end

@implementation EAECircleImageItem


-(void)modifyToBeCircleWithDiameter:(int)diameter andWithImage:(UIImage*)img{
        self.circleDiameter = diameter;
        CGFloat width = diameter;
        CGFloat height = diameter;
        self.frame = CGRectMake(0, 200, width, height);
        self.backgroundColor = [UIColor redColor];
        self.layer.cornerRadius = self.frame.size.width/2;
        self.clipsToBounds = YES;
        self.image = img;
}


@end
