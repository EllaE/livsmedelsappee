//
//  EAEFavoritesTableViewController.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-28.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//

#import "EAEFavoritesTableViewController.h"
#import "EAEFavoritesTableViewCell.h"
#import "EAEDetailFoodViewController.h"
#import "EAEFoodManager.h"

@interface EAEFavoritesTableViewController ()

@property (strong, nonatomic) EAEFoodManager * foodManager;

-(void)favoritesLoadFinished;

@end

@implementation EAEFavoritesTableViewController

//using singleton
-(EAEFoodManager*)foodManager{
    _foodManager = [EAEFoodManager sharedFoodManager];
    return _foodManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(favoritesLoadFinished) name:@"NSURLFoodDataFetchForNumberFinished" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(favoritesLoadFinished) name:@"NSFavoriteItemUpdated" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(favoritesLoadFinished) name:@"PictureUpdated" object:nil];
    
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 Method favoritesLoadFinished is called every time favorite food list is changed
 updated or loaded (at start of application). When this happen tableView is reloaded
 to show new data.
 */
-(void)favoritesLoadFinished{
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(favoritesLoadFinished) name:@"NSFavoriteItemUpdated" object:nil];
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.foodManager.allFoodList && self.foodManager.allFoodList.count >0  ){  //if no base data available, dont load favs either
        return [self.foodManager.favoriteFoodListNumbers count];
    } else return 0;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EAEFavoritesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favoriteCell" forIndexPath:indexPath];
    
    
    NSNumber * foodNr = [self.foodManager.favoriteFoodListNumbers objectAtIndex:[indexPath row]];
    EAEFood * favoriteFood = [self.foodManager searchObectWithIdNumber:foodNr];
    if(foodNr && favoriteFood){
        if(!favoriteFood.nutrientValues){
            [self.foodManager loadFoodDetailDataIfNecessaryForObj:favoriteFood];
        } else {
            
            cell.favoriteFoodLabel.text = favoriteFood.name;
            cell.favoriteKcalLabel.text =  [NSString stringWithFormat:@"%@ %@" ,@"Kcal:",[favoriteFood.nutrientValues valueForKey:@"energyKcal"] ];
            cell.favoriteCarbsLabel.text=  [NSString stringWithFormat:@"%@ %@" ,@"Carbs:",[favoriteFood.nutrientValues valueForKey:@"carbohydrates"] ];
            cell.favoriteFatLabel.text =  [NSString stringWithFormat:@"%@ %@" ,@"Fat:",[favoriteFood.nutrientValues valueForKey:@"fat"] ];
            
            if(favoriteFood.picturePath){
                
                //get image
                NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString * path = paths[0];
                NSString * picName = favoriteFood.picturePath;
                NSString * wholeImagePath = [path stringByAppendingPathComponent:picName];
                NSLog(@"Whole path: %@", wholeImagePath);
                UIImage * foodImage = [UIImage imageWithContentsOfFile:wholeImagePath];
                
                if(foodImage){
                    cell.favoriteImageView.image = foodImage;
                } else {
                    NSLog(@"Favorite tablecell: Failed to show image");
                    cell.favoriteImageView.image = [UIImage imageNamed:@"no-picture"];
                }
            } else {
                cell.favoriteImageView.image = [UIImage imageNamed:@"no-picture"];
            }
        }
        cell.viewedFood = favoriteFood;
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    EAEFavoritesTableViewCell * cell = sender;
    if([segue.identifier isEqualToString:@"foodFavoriteSegue"]){
        EAEDetailFoodViewController * foodDetailView = [segue destinationViewController];
        foodDetailView.foodItem = cell.viewedFood;
        foodDetailView.isShowingFavoriteFood = YES;
    }
    
}




@end
