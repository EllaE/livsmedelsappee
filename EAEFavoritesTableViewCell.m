//
//  EAEFavoritesTableViewCell.m
//  MyFoodstuff
//
//  Created by ITHS-Ella on 2016-02-28.
//  Copyright © 2016 ITHS-Ella. All rights reserved.
//
//  EAEFavoritesTableViewCell is used to display cell in table with more detailed
//  information which is name, image, kcal, carbs and fat levels.


#import "EAEFavoritesTableViewCell.h"

@implementation EAEFavoritesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
